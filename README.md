# Global API Policy for IBM API Connect

## Overview

This documentation outlines the setup and usage of the Global API Policy within the IBM API Connect environment. The policy enhances security and consistency of API interactions, standardizing error handling, request pre-processing, and post-response behavior.

## Implemented Policy Details

### Pre-request Global Policy
This policy manages the manipulation of specific headers to enhance API security and facilitate transaction tracking.

- **"BelGov-Eco-ConsumerID"**: Identifies API consumers.
  - Default Value: "NA" if no authentication has occurred, ensuring tracking of consumer identity.
  - Request Types:
    - **REST**: Post-authentication via API Key, the value is a combination of the consumer's organization and application identifiers.
    - **SOAP**: With WS-Security header authentication, the value is the Subject DN from the certificate used.

- **"BelGov-Eco-KeyID"**: Removed from REST requests post-authentication to protect the API key from exposure.

- **"BelGov-Eco-TransactionID"**: Required for transaction tracking; its absence triggers an immediate halt in processing and an error is generated.

### Post-response Global Policy
Enhances the security and integrity of responses delivered to API consumers.

- **"BelGov-Eco-ServiceInfo" Header Inclusion**:
  - Appended to all responses as a JSON string, detailing essential transaction information:
    - **"BelGov-Eco-TransactionID"**: The unique transaction identifier.
    - **"BelGov-Eco-ConsumerID"**: The consumer identifier ascertained during the pre-request stage.
    - **"BelGov-Eco-ProviderID"**, **"BelGov-Eco-ProviderStatus"**, **"BelGov-Eco-ProviderTime"**: Provide context on the provider identity, request status, and processing time, respectively.

  - Iclude this header ensures transparency and facilitates transaction tracing.

- **Response Header Clean**:
  - Headers such as "BelGov-Eco-ConsumerID" and "BelGov-Eco-ProviderID" are removed from the response to maintain privacy and security.

## Policy Management Commands

```sh
# Creating a Global Policy
apic global-policies:create --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog [policy_file.yaml]

# Creating a Global Policy Pre-hook
apic global-policy-prehooks:create --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog [prehook_file.yaml]

# Listing All Global Policies
apic global-policies:list-all --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog

# Updating an Existing Policy Pre-hook
apic global-policy-prehooks:update --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog [prehook_file.yaml]

# Stopping a Designated Policy Pre-hook
apic global-policy-prehooks:delete --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog

# Deleting a Global Policy
apic global-policies:delete --catalog [catalog_name] --configured-gateway-service [gateway_service_name] --org [organization_name] --server [management_endpoint_url] --scope catalog [policy_name]:[policy_version]
